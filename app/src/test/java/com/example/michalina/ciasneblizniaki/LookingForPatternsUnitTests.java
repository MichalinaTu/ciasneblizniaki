package com.example.michalina.ciasneblizniaki;

import org.junit.Test;

import static org.junit.Assert.*;

public class LookingForPatternsUnitTests {
    @Test
    public void lookForSqrt_correct() {
        String word = "abcdcda";
        LookingForPatterns lookingForPatterns = new LookingForSqrt();
        int[] pattern = lookingForPatterns.findPattern(3,word);
        assertNotNull(pattern);
        assertEquals(1, pattern[2]);
        assertEquals(1, pattern[3]);
        assertEquals(2, pattern[4]);
        assertEquals(2, pattern[5]);
    }

    @Test
    public void lookForSqrt_incorrect() {
        String word = "abcdacad";
        LookingForPatterns lookingForPatterns = new LookingForSqrt();
        int[] pattern = lookingForPatterns.findPattern(3,word);
        assertNull(pattern);
    }

    @Test
    public void lookForTwins_correct1() {
        String word = "abcdcda";
        LookingForPatterns lookingForPatterns = new LookingForCloseTwins();
        int[] pattern = lookingForPatterns.findPattern(3,word);
        assertNotNull(pattern);
        assertEquals(1, pattern[2]);
        assertEquals(1, pattern[3]);
        assertEquals(2, pattern[4]);
        assertEquals(2, pattern[5]);
    }

    @Test
    public void lookForTwins_correct2() {
        String word = "abacdbcda";
        LookingForPatterns lookingForPatterns = new LookingForCloseTwins();
        int[] pattern = lookingForPatterns.findPattern(3,word);
        assertNotNull(pattern);
        assertEquals(1, pattern[0]);
        assertEquals(1, pattern[1]);
        assertEquals(2, pattern[2]);
        assertEquals(1, pattern[3]);
        assertEquals(1, pattern[4]);
        assertEquals(2, pattern[5]);
        assertEquals(2, pattern[6]);
        assertEquals(2, pattern[7]);
    }

    @Test
    public void lookForTwins_incorrect() {
        String word = "abcdacad";
        LookingForPatterns lookingForPatterns = new LookingForCloseTwins();
        int[] pattern = lookingForPatterns.findPattern(3,word);
        assertNull(pattern);
    }
}
