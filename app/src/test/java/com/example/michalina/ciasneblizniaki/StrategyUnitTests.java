package com.example.michalina.ciasneblizniaki;

import org.junit.Test;

import static org.junit.Assert.*;

public class StrategyUnitTests {
    @Test
    public void AstrategyRandom() {
        String word = "abcdcda";
        AComputerStrategy strategy = new AComputerStrategyRandom();
        for (int i = 0; i < 10; i++) {
            int place = strategy.ChoosePlace(word);
            assertTrue(place>=0);
            assertTrue(place<=word.length());
        }

    }

    @Test
    public void AstrategyNaive1() {
        String word = "abcda";
        AComputerStrategy strategy = new AComputerStrategyNaive();
        for (int i = 0; i < 10; i++) {
            int place = strategy.ChoosePlace(word);
            assertTrue(place>=1);
            assertTrue(place<=word.length()-1);
        }
    }

    @Test
    public void AstrategyNaive2() {
        String word = "abcdcda";
        AComputerStrategy strategy = new AComputerStrategyNaive();
        for (int i = 0; i < 10; i++) {
            int place = strategy.ChoosePlace(word);
            assertTrue(place>=2);
            assertTrue(place<=word.length()-2);
        }
    }

    @Test
    public void AstrategyNaive3() {
        String word = "abcdcdadabcdabcd";
        AComputerStrategy strategy = new AComputerStrategyNaive();
        for (int i = 0; i < 10; i++) {
            int place = strategy.ChoosePlace(word);
            assertTrue(place>=4);
            assertTrue(place<=word.length()-4);
        }
    }

    @Test
    public void BstrategyRandom() {
        String word = "abcdcdadabcdabcd";
        BComputerStrategy strategy = new BComputerStrategyRandom();
        for (int i = 0; i < 10; i++) {
            char c = strategy.ChooseLetter(word,3,4);
            assertTrue(c>='a');
            assertTrue(c<='d');
        }
    }

    @Test
    public void BstrategyNaive() {
        String word = "abcdcdadabcdabcd";
        BComputerStrategy strategy = new BComputerStrategyNaive();
        for (int i = 0; i < 10; i++) {
            char c = strategy.ChooseLetter(word,3,4);
            assertNotEquals(c,'d');
            assertNotEquals(c,'c');
        }
    }

    @Test
    public void BstrategyDifficult() {
        String word = "abcdcbadabdcabcd";
        LookingForPatterns lookingForPatterns = new LookingForCloseTwins();
        BComputerStrategy strategy = new BComputerStrategyDifficult(lookingForPatterns);
        for (int i = 0; i < 10; i++) {
            char c = strategy.ChooseLetter(word,2,4);
            assertEquals(c,'a');
        }
    }




}
