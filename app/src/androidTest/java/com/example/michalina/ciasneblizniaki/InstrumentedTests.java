package com.example.michalina.ciasneblizniaki;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class InstrumentedTests {
    @Test
    public void useAppContext() {
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.michalina.ciasneblizniaki", appContext.getPackageName());
    }

    @Test
    public void SharedPreferencesTest() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext);
        String gameType = sharedPreferences.getString("gameType","");
        String playA = sharedPreferences.getString("playerA","");
        String playB = sharedPreferences.getString("playerB","");
        int alphLenght = Integer.parseInt(sharedPreferences.getString("alphabetLenght",""));
        int maxWordLenght = Integer.parseInt(sharedPreferences.getString("wordLenght",""));
        int delayTime = Integer.parseInt(sharedPreferences.getString("delay",""));

        assertEquals("twins", gameType);
        assertEquals("compEasy", playA);
        assertEquals("person", playB);
        assertEquals(4, alphLenght);
        assertEquals(15, maxWordLenght);
        assertEquals(400, delayTime);
    }
}
