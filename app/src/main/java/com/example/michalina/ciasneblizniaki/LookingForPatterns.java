package com.example.michalina.ciasneblizniaki;

public abstract class LookingForPatterns {

    public int[] pattern;

    public int[] findPattern(int p, String word){

        pattern = new int[word.length()];
        for(int i=0;i<=p;i++){
            int l0 = 1;
            if(2*l0<p+1-i)
                l0=(p-i+2)/2;
            for (int l=l0;l<=(word.length()-i)/2;l++){
                if (isPattern(word,i,l))
                    return pattern;
            }
        }
        return null;
    }

    public int[] findPattern(int p, String word, int maxlen){

        pattern = new int[word.length()];
        for(int i=0;i<=p;i++){
            int l0 = 1, lmax = maxlen;
            if(2*l0<p+1-i)
                l0=(p-i+2)/2;
            if (lmax>(word.length()-i)/2)
                lmax=(word.length()-i)/2;
            for (int l=l0;l<=lmax;l++){
                if (isPattern(word,i,l))
                    return pattern;
            }
        }
        return null;
    }

    protected abstract boolean isPattern (String word, int start, int length);

}
