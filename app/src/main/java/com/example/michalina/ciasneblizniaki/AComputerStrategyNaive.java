package com.example.michalina.ciasneblizniaki;

import java.util.Random;

public class AComputerStrategyNaive extends AComputerStrategy {
    Random random = new Random();
    @Override
    public int ChoosePlace(String word) {
        if(word.length()<3)
            return random.nextInt(word.length()+1);
        if(word.length()<6)
            return random.nextInt(word.length()-1)+1;
        if(word.length()<10)
            return random.nextInt(word.length()-3)+2;
        return random.nextInt(word.length()/2)+word.length()/4;
    }
}
