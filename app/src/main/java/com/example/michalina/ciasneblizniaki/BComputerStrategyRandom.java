package com.example.michalina.ciasneblizniaki;

import java.util.Random;

public class BComputerStrategyRandom extends BComputerStrategy {
    Random random = new Random();
    @Override
    public char ChooseLetter(String word, int i, int max) {
        return (char)('a'+random.nextInt(max));
    }
}
