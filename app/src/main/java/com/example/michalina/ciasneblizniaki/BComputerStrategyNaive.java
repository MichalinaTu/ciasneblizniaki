package com.example.michalina.ciasneblizniaki;

import java.util.Random;

public class BComputerStrategyNaive extends BComputerStrategy {
    Random random = new Random();
    @Override
    public char ChooseLetter(String word, int i, int max) {
        char a,b,c;
        if (i==0 || i==word.length()){
            if(i==0)
                a=word.charAt(i);
            else
                a=word.charAt(i-1);
            c = (char)('a'+random.nextInt(max-1));
            if(c>=a)
                c++;
        }
        else {
            c = (char) ('a' + random.nextInt(max - 2));
            a = word.charAt(i - 1);
            b = word.charAt(i);
            if (a > b) {
                a = word.charAt(i);
                b = word.charAt(i - 1);
            }
            if (c >= a)
                c++;
            if (c >= b)
                c++;
        }
        return c;
    }
}
