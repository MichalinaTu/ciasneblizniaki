package com.example.michalina.ciasneblizniaki;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class GameActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);


        GameFragment gameFragment = new GameFragment();
        getFragmentManager().beginTransaction().replace(R.id.contentLayout, gameFragment).commit();
        gameFragment.setAtributes(this);
    }

    public void EndGame (boolean A_won, String word, int[] pattern){
        if(A_won){
            Log.d("GameActiv","A won");
        } else {
            Log.d("GameActiv","B won");
        }
        EndGameFragment endGameFragment  = new EndGameFragment();
        endGameFragment.A_won = A_won;
        endGameFragment.word = word;
        endGameFragment.pattern=pattern;
        getFragmentManager().beginTransaction().replace(R.id.contentLayout, endGameFragment).commit();
    }

}
