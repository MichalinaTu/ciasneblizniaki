package com.example.michalina.ciasneblizniaki;

public class LookingForCloseTwins extends LookingForPatterns {

    @Override
    protected boolean isPattern(String word, int start, int length) {
        this.word = word;
        stop=start+2*length;
        leftLength = length;
        return isPatternBacktracking(start,start+1);
    }

    private int stop;
    private String word;
    private int leftLength;

    private boolean isPatternBacktracking (int start1, int start2){
        if (pattern[start1]!=0) {
            return isPatternBacktracking(start1 + 1, start2);
        }
        if (start1>=start2)
            start2=start1+1;
        if (start1>stop-1)
            return false;
        while(start2<stop){
            if(word.charAt(start1)==word.charAt(start2)){
                pattern[start1]=1;
                pattern[start2]=2;
                leftLength--;
                if(leftLength==0)
                    return true;
                if (isPatternBacktracking(start1+1, start2+1)){
                    return true;
                }
                pattern[start1]=0;
                pattern[start2]=0;
                leftLength++;
            }
            start2++;
        }
        return false;
    }
}
