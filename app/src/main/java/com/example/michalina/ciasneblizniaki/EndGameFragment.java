package com.example.michalina.ciasneblizniaki;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EndGameFragment extends Fragment{

    String word;

    boolean A_won;
    int[] pattern;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("GameFrag","onCreateView");
        View view = inflater.inflate(R.layout.fragment_endgame, container, false);
        ((TextView)view.findViewById(R.id.textViewEndGame2)).setText(A_won? "Wygrał gracz A":"Wygrał gracz B");
        LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.linearLayoutEndGameWord);
        TextView textView;
        if(A_won){
            for(int i=0;i<word.length();i++) {
                textView = new TextView(view.getContext());
                textView.setText(word.substring(i,i+1));
                if(pattern[i]==0){
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                            (int)(getResources().getDimension(R.dimen.bigTextsize)));
                } else{
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                            (int)(getResources().getDimension(R.dimen.bigbigbigTextsize)));
                    if(pattern[i]==1){
                        textView.setTextColor(getResources().getColor(R.color.FirstWord));
                    } else{
                        textView.setTextColor(getResources().getColor(R.color.SecondWord));
                    }
                }
                linearLayout.addView(textView);
            }
        } else{
            textView = new TextView(view.getContext());
            textView.setText(word);
            textView.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            textView.setTextSize(23);
            linearLayout.addView(textView);
        }
        return view;
    }

    public EndGameFragment(){}

}
