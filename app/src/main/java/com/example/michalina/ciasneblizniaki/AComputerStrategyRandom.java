package com.example.michalina.ciasneblizniaki;

import java.util.Random;

public class AComputerStrategyRandom extends AComputerStrategy {
    Random random = new Random();
    @Override
    public int ChoosePlace(String word) {
        return random.nextInt(word.length()+1);
    }
}
