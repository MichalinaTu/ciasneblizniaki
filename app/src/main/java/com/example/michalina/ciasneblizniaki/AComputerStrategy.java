package com.example.michalina.ciasneblizniaki;

public abstract class AComputerStrategy {
    abstract public int ChoosePlace(String word);
}
