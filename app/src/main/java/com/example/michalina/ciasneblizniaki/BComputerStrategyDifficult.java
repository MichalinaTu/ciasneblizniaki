package com.example.michalina.ciasneblizniaki;

import java.util.Random;

public class BComputerStrategyDifficult extends BComputerStrategy {
    Random random = new Random();

    LookingForPatterns lookingForPatterns;

    public BComputerStrategyDifficult(LookingForPatterns lookingForPatterns){
        this.lookingForPatterns = lookingForPatterns;
    }

    @Override
    public char ChooseLetter(String word, int i, int max) {
        boolean[] makesPattern = new boolean[max];
        int trueCount = 0;
        if(i==0) {
            makesPattern[word.charAt(0) - 'a'] = true;
            trueCount++;
        }
        else if (i==word.length()) {
            makesPattern[word.charAt(word.length() - 1) - 'a'] = true;
            trueCount++;
        }
        else{
            makesPattern[word.charAt(i)-'a']=true;
            makesPattern[word.charAt(i-1)-'a']=true;
            trueCount+=2;
        }
        int j;
        char c;
        do{
            j=random.nextInt(max);
            c = (char)('a'+j);
            if(!makesPattern[j]){
                String newWord = new String(word.substring(0,i)+c+word.substring(i,word.length()));
                makesPattern[j]=(lookingForPatterns.findPattern(i,newWord,2)!=null);
                if(makesPattern[j]) trueCount++;
            }
        } while (makesPattern[j] && trueCount<max);

        return c;
    }
}
