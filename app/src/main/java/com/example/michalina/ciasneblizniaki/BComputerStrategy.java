package com.example.michalina.ciasneblizniaki;

public abstract class BComputerStrategy {
    abstract public char ChooseLetter(String word, int i, int max);
}
