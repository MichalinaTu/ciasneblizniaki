package com.example.michalina.ciasneblizniaki;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;

public class GameFragment extends Fragment {

    private boolean isWorking;

    int alphLenght;
    int maxWordLenght;

    boolean isAaPerson;
    boolean isBaPerson;

    GameActivity parent;

    String word;
    boolean A_turn;
    int chosenPlace;

    LookingForPatterns lookingForPatterns;
    AComputerStrategy Astrategy;
    BComputerStrategy Bstrategy;
    private int delayTime;
    private TextView[] places;
    private TextView[] letters;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("GameFrag", "onCreate");
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("GameFrag","onCreateView");
        View view = inflater.inflate(R.layout.fragment_game, container, false);
        return view;
    }

    @Override
    public void onPause(){
        super.onPause();
        isWorking = false;
        Parameters parameters = new Parameters(word, A_turn, chosenPlace);
        File file = new File(getView().getContext().getFilesDir(),"dane.csv");
        FileHelper.WriteData(file,parameters);
        Log.d("GameFrag","onPause");
        Log.d("GameFrag","isWorking: "+isWorking);
    }

    @Override
    public void onResume(){
        super.onResume();
        isWorking = true;
        Log.d("GameFrag","onResume");
        Log.d("GameFrag","isWorking: "+isWorking);
        getSettings();
        File file = new File(getView().getContext().getFilesDir(),"dane.csv");
        Parameters parameters = FileHelper.ReadData(file);
        word = parameters.word;
        setWord();
        if(parameters.isATurn){
            Amove();
        }
        else{
            placeChosen(parameters.place);
            Bmove();
        }
    }

    public GameFragment( ){
    }

    public void setAtributes(GameActivity par){
        parent = par;
    }

    private void getSettings(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getView().getContext());
        alphLenght = Integer.parseInt(sharedPreferences.getString("alphabetLenght","3"));
        maxWordLenght = Integer.parseInt(sharedPreferences.getString("wordLenght","5"));
        delayTime = Integer.parseInt(sharedPreferences.getString("delay","400"));

        String game = sharedPreferences.getString("gameType","sqrt");
        if(game.equalsIgnoreCase("sqrt"))
            lookingForPatterns = new LookingForSqrt();
        else
            lookingForPatterns = new LookingForCloseTwins();

        String playA = sharedPreferences.getString("playerA","compRand");
        String playB = sharedPreferences.getString("playerB","person");
        isAaPerson = (playA.equalsIgnoreCase("person"));
        isBaPerson = (playB.equalsIgnoreCase("person"));
        if (!isBaPerson){
            Bstrategy = new BComputerStrategyRandom();
            if (playB.equalsIgnoreCase("compEasy"))
                Bstrategy = new BComputerStrategyNaive();
            else if (playB.equalsIgnoreCase("compDifficult"))
                Bstrategy = new BComputerStrategyDifficult(lookingForPatterns);
        }
        if (!isAaPerson){
            Astrategy = new AComputerStrategyRandom();
            if (playB.equalsIgnoreCase("compEasy"))
                Astrategy = new AComputerStrategyNaive();
        }

    }

    private void Amove (){
        A_turn = true;
        setTexts();
        //if person
        if(!isAaPerson){
            int i = Astrategy.ChoosePlace(word);
            placeChosen(i);

            CountDownTimer countDownTimer = new CountDownTimer(delayTime, 100) {

                @Override
                public void onTick(long millisUntilFinished) { }

                @Override
                public void onFinish() {
                    if(isWorking)
                        AmoveEnd();
                }
            }.start();
        }
    }

    private void AmoveEnd(){
        Bmove();
    }

    private void placeChosen(int i) {
        chosenPlace = i;
        places[i].setTextColor(getResources().getColor(R.color.colorAccent));
    }

    private void placeClicked(int i){
        if(isAaPerson && A_turn){
            placeChosen(i);
            AmoveEnd();
        }
    }


    private void Bmove(){
        A_turn = false;
        setTexts();
        if(!isBaPerson){
            final char c = Bstrategy.ChooseLetter(word,chosenPlace, alphLenght);
            letters[c-'a'].setTextColor(getResources().getColor(R.color.colorAccent));
            new CountDownTimer(delayTime, 100) {

                @Override
                public void onTick(long millisUntilFinished) { }

                @Override
                public void onFinish() {
                    if (isWorking){
                        letterChosen(c);
                        new CountDownTimer(delayTime, 100) {

                            @Override
                            public void onTick(long millisUntilFinished) { }

                            @Override
                            public void onFinish() {
                                if(isWorking){
                                    BmoveEnd();
                                }
                            }
                        }.start();
                    }

                }
            }.start();
        }
    }

    private void BmoveEnd(){
        int[] pattern = lookingForPatterns.findPattern(chosenPlace,word);
        if(pattern!=null){
            parent.EndGame(true,word,pattern);
        }
        else {
            if (word.length() == maxWordLenght) {
                parent.EndGame(false, word, null);
            } else {
                setTexts();
                Amove();
            }
        }
    }

    private void letterChosen(char c){
        word = new String(word.substring(0,chosenPlace)+c+word.substring(chosenPlace,word.length()));
        setWord();
    }

    private void letterClicked(char c){
        if(isBaPerson && !A_turn){
            letterChosen(c);
            BmoveEnd();
        }
    }

    private void setTexts() {
        LinearLayout layout = (LinearLayout)getView().findViewById(R.id.alphabetLayout);
        TextView textViewPlayer = (TextView)getView().findViewById(R.id.textViewPlayer);
        TextView textViewAction = (TextView)getView().findViewById(R.id.textViewAction);
        letters = new TextView[alphLenght];
        if(A_turn){
            textViewPlayer.setText("Ruch gracza A:");
            textViewAction.setText("wybierz miejsce");
            layout.removeAllViews();
        } else{
            textViewPlayer.setText("Ruch gracza B:");
            textViewAction.setText("wybierz literę");
            for (int i = 0; i < alphLenght; i++) {
                final char c = (char)(i+'a');
                TextView textView = new TextView(getView().getContext());
                //textView.setTextSize(p/4);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                        (int)(getResources().getDimension(R.dimen.bigbigTextsize)));
                textView.setText("  " + c + "  ");
                textView.setClickable(true);
                letters[i]=textView;
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        letterClicked(c);
                    }
                });
                layout.addView(textView);
            }
        }

    }
    private int getWidht(){
        WindowManager wm = (WindowManager) getView().getContext().getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        wm.getDefaultDisplay().getSize(size);
        int w = size.x;
        //Toast.makeText(getActivity(), "GameFrag widht = "+w, Toast.LENGTH_LONG).show();
        Log.d("GameFrag","widht = "+w);
        return w;
    }

    private void setWord() {
        LinearLayout[] layouts = {(LinearLayout) getView().findViewById(R.id.wordLayout),
                (LinearLayout) getView().findViewById(R.id.wordLayout2),
                (LinearLayout) getView().findViewById(R.id.wordLayout3),
                (LinearLayout) getView().findViewById(R.id.wordLayout4)};
        for (int k = 0; k < 4; k++)
            layouts[k].removeAllViews();
        TextView textView;
        places = new TextView[word.length()+1];

        double scale=1;
        if (word.length()<4){
            scale = 2;
        }
        else if (word.length()<12){//(word.length()<7){
            scale = 1.8;
        }
        else if (word.length()<21){//(word.length()<7){
            scale = 1.6;
        }
        else if (word.length()<24){//(word.length()<8){
            scale = 1.41;
        }
        else if (word.length()<27){//(word.length()<9){
            scale = 1.25;
        }
        else if (word.length()<30){//(word.length()<9){
            scale = 1.13;
        }

        textView = new TextView(getView().getContext());
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                (int)(getResources().getDimension(R.dimen.normalTextsize)*scale));
        Log.d("GameFrag","text size = "+textView.getTextSize());
        //textView.setTextSize((int)(10*scale));
        textView.setText("    " + 0 + "    ");
        textView.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        textView.setClickable(true);
        places[0] = textView;
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                placeClicked(0);
            }
        });
        layouts[0].addView(textView);

        //int p=getWidht()*9/(int)(700*scale);
        int p=getWidht()*9/(int)(46*textView.getTextSize());
        Log.d("GameFrag","length = "+word.length()+", scale = "+scale+", p = "+p);

        for (int k = 0; k < 4; k++) {
            for (int i = 0+k*p; i < word.length() && i<p+k*p; i++) {
                textView = new TextView(getView().getContext());
                textView.setText(word.substring(i, i + 1));
                textView.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                        (int)(getResources().getDimension(R.dimen.bigbigTextsize)*scale));
                //textView.setTextSize((int)(23*scale));
                //Log.d("GameFrag","text size = "+textView.getTextSize());
                layouts[k].addView(textView);

                final int j = i+1;
                textView = new TextView(getView().getContext());
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                        (int)(getResources().getDimension(R.dimen.normalTextsize)*scale));
                //textView.setTextSize((int)(10*scale));
                //Log.d("GameFrag","text size = "+textView.getTextSize());
                textView.setText("    " + j + "    ");
                textView.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                textView.setClickable(true);
                places[j]=textView;
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        placeClicked(j);
                    }
                });
                layouts[k].addView(textView);
            }
        }
    }

}
