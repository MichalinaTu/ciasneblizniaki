package com.example.michalina.ciasneblizniaki;

public class LookingForSqrt extends LookingForPatterns {
    @Override
    protected boolean isPattern(String word, int start, int length) {
        int k;
        for(k=0;k<length;k++){
            if(word.charAt(start+k)!=word.charAt(start+k+length))
                break;
        }
        if (k==length){
            for(int i=0;i<length;i++){
                pattern[i+start]=1;
                pattern[i+length+start]=2;
            }
            return true;
        }
        return false;
    }
}
