package com.example.michalina.ciasneblizniaki;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.File;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    public void onNewGameClick(View view){
        Log.d("StartActivity","Click NewGame");
        File file = new File(getFilesDir(),"dane.csv");
        FileHelper.WriteData(file,null);
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
        /*
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
         */
    }

    public void onSettingsClick(View view){
        Log.d("StartActivity","Click Settings");
        Intent intent = new Intent(this, PrefsActiv.class);
        startActivityForResult(intent, 0);
    }

    public void onInstructionsClick(View view){
        Log.d("StartActivity","Click Instructions");
        Intent intent = new Intent(this, InstructionsActivity.class);
        startActivityForResult(intent, 0);
    }
}
