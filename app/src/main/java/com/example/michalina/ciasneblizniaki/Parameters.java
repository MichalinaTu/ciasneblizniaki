package com.example.michalina.ciasneblizniaki;

public class Parameters {
    public String word;
    public boolean isATurn;
    public int place;

    public Parameters() {
        this.word = "a";
        this.isATurn = true;
        this.place = 0;
    }

    public Parameters(String word, boolean isATurn, int place) {
        this.word = word;
        this.isATurn = isATurn;
        this.place = place;
    }

    public String GetIsATurn (){
        if(isATurn)
            return "A";
        return "B";
    }

    public void SetIsATurn (String s){
        isATurn = (s.charAt(0)=='A');
    }
}
