package com.example.michalina.ciasneblizniaki;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FileHelper {

    public static void WriteData(File file, Parameters params){
        try {
            FileOutputStream fout = new FileOutputStream(file);
            if(params!=null)
                fout.write((params.word+","+params.GetIsATurn()+","+params.place+"\n").getBytes());
            fout.close();
        }
        catch (FileNotFoundException e){
            Log.d("fileEx","File not found exception "+e.getMessage());
        }
        catch (IOException e){
            Log.d("IOEx","IO exception "+e.getMessage());
        }
    }

    public static Parameters ReadData(File file){
        Parameters params = new Parameters();
        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line="";
            String splitter=",";
            //FileInputStream fout = new FileInputStream(file);
            if((line=br.readLine())!=null){
                String[] s = line.split(splitter);
                if(s.length>0){
                    params.word=s[0];
                    params.SetIsATurn(s[1]);
                    params.place = Integer.parseInt(s[2]);
                }
            }
            fr.close();
            br.close();
        }
        catch (FileNotFoundException e){
            Log.d("fileEx","File not found exception "+e.getMessage());
        }
        catch (IOException e){
            Log.d("IOEx","IO exception "+e.getMessage());
        }
        return params;
    }

}
